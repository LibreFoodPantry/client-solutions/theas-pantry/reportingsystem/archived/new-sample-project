// Austin Engel

var express = require('express');
var bodyParser = require('body-parser');
var todoController = require("./controllers/todoController");

var app = express();

//setting template engine
app.set('view engine', 'ejs');

//static files
app.use(express.static('./public'));

//fire controllers
todoController(app);

app.listen(3000, ()=>{console.log('Listening to port 3000')});