var express = require('express');
var app = express();

app.set('view engine', 'ejs');

app.use('/Assets', express.static('Assets'));


app.get('/', function(req, res){
    res.render('index');
});
app.get('/contact', function(req, res){
    res.render(`contact.ejs`);
});
app.get('/profile/:name', function(req, res){
    var data = {age: 29, job: 'ninja', hobbies: ['eating', 'fighting', 'fishing']};
    var skills = ['Reverse Lotus', 'Shadowjutsu', 'Drunken Fist'];
    res.render('profile', {person: req.params.name, data: data, skills:skills});
});

app.listen(3000, ()=>{console.log("Running on port 3000")});
