//Austhor: Austin Engel

//Load first party libraries
const http = require('http');
const recieve = require('./subProcesses/recieve');

//Load 3rd party Libraries
const express = require('express');

//Creates express app
var app = express();

//fire sub proccess
recieve(app);

//Starting server, listening to port 3000
app.listen(3000, ()=>{console.log('Listening to port 3000')});