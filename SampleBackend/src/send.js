const amqp = require('amqplib/callback_api');

//TEST, DELETE BEFORE USE
var data = {
    name: 'Austin Engel',
    company: 'JP Morgan',
    designation: 'Senior Application Engineer'
};

//create connection
amqp.connect('amqp://localhost', function(connError, connection){
    if(connError) throw connError;
    //create channel
    connection.createChannel(function(channError, channel){
        if(channError) throw channError;
        //assert queue
        const queue = 'test'
        channel.assertQueue(queue);

        //send message to queue
        channel.sendToQueue(queue, Buffer.from(JSON.stringify(data)));
        console.log(`message sent to ${queue}`);
    });
})